/**
 * @file fast_trie_iterator.tcc
 * @author Sean Massung
 */

/**
 * Provides forward iteration through this fast_trie, saving current state via
 * a std::stack. As iteration progresses through the trie, characters are added
 * and removed from the current key.
 */
class iterator: public std::iterator<std::forward_iterator_tag,
                                     std::pair<const std::string, T &>>
{
    public:
        friend fast_trie;
        typedef iterator self_type;
        typedef std::pair<const std::string, T &> value_type;
        typedef value_type & reference;
        typedef value_type* pointer;
        typedef std::forward_iterator_tag iterator_category;
        typedef ptrdiff_t difference_type;

    private:
        /**
         * Simple class to keep track of the current state of iteration. All
         * that's needed is a pointer ot the current node and the current
         * character sequence that makes up a key.
         */
        struct state
        {
            /**
             * Constructor.
             * @param p_n The current node
             * @param p_w The current key (word)
             */
            state(const node* p_n, const std::string & p_w): n{p_n}, w{p_w}
                { /* nothing */ }

            /**
             * Equality operator (needed for std::stack<state>)
             * @param a
             * @param b
             * @return whether the two states refer to the same state
             */
            friend bool operator==(const state & a, const state & b)
            {
                return a.n == b.n && a.w == b.w;
            }

            const node* n;
            std::string w;
        };

        // this is a shared_ptr because iterators need to be copy-constructable
        std::shared_ptr<value_type> _val;

        std::stack<state> _states;

        /**
         * Private constructor for the fast_trie class to use.
         * @param cur The node to create this iterator at -- this is most
         * likely the root.
         */
        iterator(const node* cur)
        {
            if(cur != nullptr)
            {
                // add children without including this node's char value
                auto it = cur->_children.rbegin();
                for( ; it != cur->_children.rend(); ++it)
                    _states.emplace(*it, "");

                inc_helper();
            }
        }

    public:
        /**
         * Default constructor: _states is empty and _val is nullptr.
         */
        iterator() { /* nothing */ }

        /**
         * Exception-safe assignment operator.
         * @param other The iterator to copy
         * @return a reference to the current iterator
         */
        self_type & operator=(iterator other)
        {
            std::swap(*this, other);
            return *this;
        }

        /**
         * Helper function to add the children of the current node in the
         * correct order.
         */
        void add_children()
        {
            state old_top = _states.top();
            _states.pop();
            auto it = old_top.n->_children.rbegin();
            for( ; it != old_top.n->_children.rend(); ++it)
                _states.emplace(*it, old_top.w + old_top.n->_ch);
        }

        /**
         * Recursive helper for operator++, adds states to the stack until
         * a value is found.
         */
        void inc_helper()
        {
            if(_states.empty() || _states.top().n->_value != nullptr)
                return;

            add_children();
            inc_helper();
        }

        /** pre-increment */
        self_type & operator++()
        {
            if(_states.empty())
                return *this;

            add_children();
            inc_helper();
            return *this;
        }

        /** post-increment */
        self_type operator++(int)
        {
            iterator save{*this};
            ++(*this);
            return save;
        }

        /** equality */
        bool operator==(const iterator & other)
        {
            return _states == other._states;
        }

        /** inequality */
        bool operator!=(const iterator & other)
        {
            return !(*this == other);
        }

        /** dereference operator */
        reference operator*()
        {
            _val = std::make_shared<value_type>(
                _states.top().w + _states.top().n->_ch,
                *_states.top().n->_value
            );
            return *_val;
        }

        /** arrow operator */
        pointer operator->()
        {
            _val = std::make_shared<value_type>(
                _states.top().w + _states.top().n->_ch,
                *_states.top().n->_value
            );
            return _val;
        }
};

/**
 * @return an iterator to the beginning of this fast_trie
 */
iterator begin() const
{
    return iterator{_root};
}

/**
 * @return an iterator to the end of this fast_trie
 */
iterator end() const
{
    return iterator{nullptr};
}
