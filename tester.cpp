/**
 * @file tester.cpp
 * @author Sean Massung
 */

#include <chrono>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <unordered_map>
#include "fast_trie.h"

void simple_test()
{
    fast_trie<int> t;

    t.insert("cat", 3);

    t.insert("armadillo", 123);
    t.insert("car", 9);
    t.insert("cabbage", 27);
    t["car"] += 3; // modify old
    t["new one"] = 100; // insert new

    fast_trie<int> other = t;

    std::cout << "3 " << t.at("cat") << std::endl;
    std::cout << "0 " << t["dogs"] << std::endl;
    std::cout << "12 " << t["car"] << std::endl;
    std::cout << "27 " << t.at("cabbage") << std::endl;
    std::cout << "123 " << t.at("armadillo") << std::endl;
    std::cout << "100 " << t.at("new one") << std::endl;
    std::cout << "0 " << t["nonexistent"] << std::endl; // default value

    std::cout << "----" << std::endl;

    std::cout << other.at("cat") << std::endl;
    std::cout << other["car"] << std::endl;
    std::cout << other.at("cabbage") << std::endl;
    std::cout << other.at("armadillo") << std::endl;
    std::cout << other.at("new one") << std::endl;
    std::cout << other["nonexistent"] << std::endl; // default value

    std::cout << "----" << std::endl;

    std::cout << t << std::endl;
    for(auto & p: t)
        std::cout << p.first << ": " << p.second << std::endl;
}

template <class Map, class Duration = std::chrono::milliseconds>
Duration timed_test()
{
    auto start = std::chrono::steady_clock::now();

    Map m;
    std::ifstream input{"tokenized.txt"};
    std::string word;
    while(input >> word)
        ++m[word];

    /*
    using pair_t = std::pair<std::string, uint64_t>;
    std::vector<pair_t> sorted{m.begin(), m.end()};
    m.clear();
    std::sort(sorted.begin(), sorted.end(),
        [](const pair_t & a, const pair_t & b) {
            return a.first < b.first;
        }
    );
    */

    auto end = std::chrono::steady_clock::now();

    for(auto & p: m)
        std::cout << p.first << ": " << p.second << std::endl;

    return std::chrono::duration_cast<Duration>(end - start);
}

int main(int argc, char* argv[])
{
    simple_test();

    /*
    auto m_ms = timed_test<std::map<std::string, uint64_t>>();
    std::cout << "std::map: " << m_ms.count() << std::endl;

    auto u_ms = timed_test<std::unordered_map<std::string, uint64_t>>();
    std::cout << "std::unordered_map: " << u_ms.count() << std::endl;

    auto t_ms = timed_test<fast_trie<uint64_t>>();
    std::cout << "fast_trie: " << t_ms.count() << std::endl;
    */
}
