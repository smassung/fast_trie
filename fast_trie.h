/**
 * @file fast_trie.h
 * @author Sean Massung
 */

#ifndef _FAST_TRIE_H_
#define _FAST_TRIE_H_

#include <algorithm>
#include <iostream>
#include <memory>
#include <stack>
#include <string>
#include <vector>

/**
 * A string to T dictionary data structure. The keys are stored in a compacted,
 * sorted order for fast insertion and retrieval.
 */
template <class T>
class fast_trie
{
    public:
        /**
         * Constructor.
         */
        fast_trie();

        /**
         * Destructor.
         */
        ~fast_trie();

        /**
         * Copy constructor.
         * @param other The fast_trie to copy
         */
        fast_trie(const fast_trie & other);

        /**
         * Assignment operator.
         * @param rhs The fast_trie to assign into this one
         */
        fast_trie & operator=(fast_trie rhs);

        /**
         * Move constructor.
         */
        fast_trie(fast_trie && other);

        /**
         * @param other The trie to swap with
         */
        void swap(fast_trie & other);

        /**
         * @param key
         * @param value
         */
        void insert(const std::string & key, const T & value);

        /**
         * Bracket operator; returns a reference to the value associated with
         * the given key. If the key does not exist, it is inserted and its
         * value is the default value type.
         * @param key
         * @return reference to value for given key
         */
        T & operator[](const std::string & key);

        /**
         * @param key
         * @return a reference to the value associated with the parameter
         * Throws an exception if the key doesn't exist.
         */
        T & at(const std::string & key);

        /**
         * at() const version.
         * @param key
         * @return a reference to the value associated with the parameter
         * Throws an exception if the key doesn't exist.
         */
        const T & at(const std::string & key) const;

        /**
         * @return the number of elements in this fast_trie
         */
        uint64_t size() const;

    private:
        /**
         * The internal structure of the fast_trie. Each node represents a
         * character in each of the key strings. If the _value member is not
         * nullptr, then the current node is the end of a key. A node is a leaf
         * if the _children vector is empty.
         */
        struct node
        {
            /**
             * Default constructor.
             */
            node(char ch):
                _value{nullptr}, _ch{ch}
            { /* nothing */ }

            /**
             * Parameter constructor.
             * @param value
             */
            node(const T & value, char ch):
                _value{new T{value}}, _ch{ch}
            { /* nothing */ }

            /**
             * @param ch The character to search for in the children vector
             * @return a pointer to the child containing that character or
             * nullptr if it doesn't exist
             */
            node* get_child(char ch)
            {
                int lo = 0;
                int hi = _children.size() - 1;

                // deferred detection of equality (only one conditional per
                // iteration)
                while(lo < hi)
                {
                    int mid = (lo + hi) / 2;
                    if(_children[mid]->_ch < ch)
                        lo = mid + 1;
                    else
                        hi = mid;
                }

                if(hi == lo && _children[lo]->_ch == ch)
                    return _children[lo];
                return nullptr;
            }

            /**
             * @param ch The character to insert into the children vector
             * @return a pointer to the new child node
             */
            node* insert(char ch)
            {
                size_t i = 0;
                for( ; i < _children.size(); ++i)
                    if(_children[i]->_ch > ch)
                        break;
                return *_children.emplace(_children.begin() + i, new node{ch});
            }

            /** the value stored at this node or nullptr if not a key */
            T* _value;

            /** the character value at this node */
            char _ch;

            /** children of this character node */
            std::vector<node*> _children;
        };

        /** the beginning of this fast_trie */
        node* _root;

        /** the number of elements stored in this fast_trie */
        uint64_t _size;

        /**
         * @param cur
         */
        void clear(node* cur);

        /**
         * Makes a deep copy from the second paramter into the first.
         * @param cur
         * @param other
         */
        void copy(node* & cur, const node* const other);

        /**
         * @param key
         * @return a reference to the value for the parameter
         */
        T & at_helper(const std::string & key) const;

        /**
         * @param key
         * @param value
         * @return reference to the value for the given key
         */
        T & insert_helper(const std::string & key, const T & value);

    public:

        #include "fast_trie_iterator.tcc"

        /**
         * Recursive helper for printing to streams.
         */
        static void printer(std::ostream & out, const node* cur, size_t depth)
        {
            out << std::string(depth, ' ') << cur->_ch;
            if(cur->_value != nullptr)
                out << " " << *cur->_value;

            out << std::endl;
            for(auto & n: cur->_children)
                printer(out, n, depth + 1);
        }

        /**
         * @param out
         * @param trie
         * @return a reference to the output stream
         */
        friend std::ostream & operator<<(std::ostream & out,
                                         const fast_trie & trie)
        {
            fast_trie::printer(out, trie._root, 0);
            return out;
        }

        /**
         * Basic exception class.
         */
        class fast_trie_exception: public std::exception
        {
            public:
                fast_trie_exception(const std::string & error):
                    _error{error} { /* nothing */ }

                const char* what() const throw ()
                {
                    return _error.c_str();
                }

            private:
                std::string _error;
        };
};

#include "fast_trie.tcc"
#endif
