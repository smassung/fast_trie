/**
 * @file fast_trie.tcc
 * @author Sean Massung
 */

#include "fast_trie.h"

template <class T>
fast_trie<T>::fast_trie():
    _root{new node{' '}}, _size{0}
{ /* nothing */ }

template <class T>
fast_trie<T>::~fast_trie()
{
    clear(_root);
}

template <class T>
fast_trie<T>::fast_trie(const fast_trie & other):
    _size{other._size}
{
    copy(_root, other._root);
}

template <class T>
fast_trie<T> & fast_trie<T>::operator=(fast_trie rhs)
{
    swap(rhs);
    return *this;
}

template <class T>
fast_trie<T>::fast_trie(fast_trie && other):
    _root{std::move(other._root)},
    _size{std::move(other._size)}
{ /* nothing */ }

template <class T>
void fast_trie<T>::copy(node* & cur, const node* const other)
{
    if(other == nullptr)
    {
        cur = nullptr;
        return;
    }

    cur = new node{other->_ch};
    if(other->_value != nullptr)
        cur->_value = new T{*other->_value};

    cur->_children.resize(other->_children.size(), nullptr);
    for(size_t i = 0; i < other->_children.size(); ++i)
        copy(cur->_children[i], other->_children[i]);
}

template <class T>
void fast_trie<T>::swap(fast_trie & other)
{
    std::swap(_root, other._root);
    std::swap(_size, other._size);
}

template <class T>
void fast_trie<T>::insert(const std::string & key, const T & value)
{
    insert_helper(key, value);
}

template <class T>
T & fast_trie<T>::operator[](const std::string & key)
{
    return insert_helper(key, T{});
}

template <class T>
T & fast_trie<T>::at(const std::string & key)
{
    return at_helper(key);
}

template <class T>
const T & fast_trie<T>::at(const std::string & key) const
{
    return at_helper(key);
}

template <class T>
uint64_t fast_trie<T>::size() const
{
    return _size;
}

template <class T>
void fast_trie<T>::clear(node* cur)
{
    if(cur == nullptr)
        return;

    for(size_t i = 0; i < cur->_children.size(); ++i)
        clear(cur->_children[i]);

    delete cur->_value;
    delete cur;
}

template <class T>
T & fast_trie<T>::at_helper(const std::string & key) const
{
    node* cur = _root;
    for(size_t i = 0; i < key.size(); ++i)
    {
        node* kid = cur->get_child(key[i]);
        if(kid == nullptr)
            throw fast_trie_exception{"1 key doesn't exist! (" + key + ")"};

        if(i == key.size() - 1)
        {
            if(kid->_value == nullptr)
                throw fast_trie_exception{"2 key doesn't exist! (" + key + ")"};
            return *kid->_value;
        }

        cur = kid;
    }
    
    throw fast_trie_exception{"3 key doesn't exist! (" + key + ")"};
}

template <class T>
T & fast_trie<T>::insert_helper(const std::string & key, const T & value)
{
    node* cur = _root;
    for(size_t i = 0; i < key.size(); ++i)
    {
        node* kid = cur->get_child(key[i]);
        
        if(kid == nullptr) // "set" this char as used
            kid = cur->insert(key[i]);
        
        if(i == key.size() - 1) // if at end, insert
        {
            if(kid->_value != nullptr) // key already exists!
                return *kid->_value;

            kid->_value = new T{value};
            ++_size;
            return *kid->_value;
        }

        cur = kid; // advance down
    }

    throw fast_trie_exception{"logic error"};
}
