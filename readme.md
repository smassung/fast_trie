# fast_trie

`fast_trie` is a generic string trie dictionary data structure. It can map
std::strings to any value type.

# Building

Initially,

```bash
mkdir build
cd build
CXX=clang++ cmake .. -DCMAKE_BUILD_TYPE=Debug
```

Then simply

```bash
make
```
